/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import { createStore, applyMiddleware,compose} from 'redux'; //NEW
import { Navigation } from "react-native-navigation";//NEW
import thunkMiddleware from 'redux-thunk'; //NEW
import logger from 'redux-logger' //NEW
import { registerScreens } from "./app/containers/screens"; //NEW
import { Provider } from 'react-redux' //NEW
import reducer from './app/reducers';

function configureStore(initialState){
  const enhancer = compose(
    applyMiddleware(
      thunkMiddleware,
      logger
    )
  );
  return createStore(reducer, initialState, enhancer);
}

const store = configureStore({});

registerScreens(store,Provider);

export default class App {
  
  constructor() {
    Navigation.events().registerAppLaunchedListener(() => {
      Navigation.setDefaultOptions({
        layout: {
          orientation: ['portrait']
        },
        topBar: {
          visible: false,
          drawBehind: true
        },
      });
      Navigation.setRoot({
        root: {
          component: {
            name: "map",
            id:'map'
          }
        }
      });
    });
  }
}