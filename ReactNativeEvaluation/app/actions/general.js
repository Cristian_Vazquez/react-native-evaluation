import * as type from './types';
import Api from '../lib/api';

export function dataWeather(estado){
    return (dispatch, getState) => {
        let strRuta = '';
        if (estado == 'Aguascalientes'){
            strRuta = 'https://api.openweathermap.org/data/2.5/weather?q=Aguascalientes,Mexico&appid=6dbaec278c8f6da606c4eafca51e75a5&units=metric'
        }else if (estado == 'Leon'){
            strRuta = 'https://api.openweathermap.org/data/2.5/weather?q=Leon,Mexico&appid=6dbaec278c8f6da606c4eafca51e75a5&units=metric'
        }else if (estado == 'Irapuato'){
            strRuta = 'https://api.openweathermap.org/data/2.5/weather?q=Irapuato,Mexico&appid=6dbaec278c8f6da606c4eafca51e75a5&units=metric'
        }else if (estado == 'Guadalajara'){
            strRuta = 'https://api.openweathermap.org/data/2.5/weather?q=Guadalajara,Mexico&appid=6dbaec278c8f6da606c4eafca51e75a5&units=metric'
        }else if (estado == 'Morelia'){
            strRuta = 'https://api.openweathermap.org/data/2.5/weather?q=Morelia,Mexico&appid=6dbaec278c8f6da606c4eafca51e75a5&units=metric'
        }
          
        return Api.get('General', strRuta).then(resp => { 
            let mapResponse = resp;
            dispatch (setResponseDataWeather({mapResponse}))
        }).catch((ex)=>{
            dispatch (setResponseDataWeather({}));
            console.log(ex);
        })
    }
}

export function setResponseDataWeather({mapResponse})
{
    return {
        type: type.SET_MAP_RESPONSE,
        mapResponse
    }
}

export function setCleanState() {
    return {
        type: type.SET_CLEAN
    }
}