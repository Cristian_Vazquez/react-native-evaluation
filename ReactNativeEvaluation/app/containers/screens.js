import {Navigation} from 'react-native-navigation';
import Mapa from './map'
import List from './list'
import Details from './details'

export function registerScreens(store,provider) {
    Navigation.registerComponentWithRedux('map',()=>Mapa,provider,store);
    Navigation.registerComponentWithRedux('list',()=>List,provider,store);
    Navigation.registerComponentWithRedux('details',()=>Details,provider,store);
}