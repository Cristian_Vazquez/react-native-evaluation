import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Navigation } from 'react-native-navigation';
import MapView, { PROVIDER_GOOGLE } from 'react-native-maps';
import {Appbar} from 'react-native-paper';
import { ActionCreators } from '../actions/index';
import { bindActionCreators } from 'redux';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  BackHandler,
  Dimensions,
  TouchableOpacity,
} from 'react-native';


const { width, height } = Dimensions.get('window');


const ASPECT_RATIO = width / height;
const LATITUDE = 21.8347744;
const LONGITUDE = -102.39509;
const LATITUDE_DELTA = 9.96;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;


class Mapa extends Component {

  _handleList = () => this.fnMoveToList();

    static propTypes = {
        componentId: PropTypes.string
    };


    constructor(props) {
        super(props);

        this.state = {
          pinSelected: false,
          estado: '',
        }
        
        Navigation.mergeOptions(this.props.componentId, {
            topBar: {
                visible: false,
                drawBehind: false
            }
        });
    }

    
    

    componentDidMount() {
      
    }

    componentWillUnmount() {
      if (Platform.OS == 'android') {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
      }
      if (this.navigationEventListener) {
          this.navigationEventListener.remove();
      }
    }

    componentDidAppear() {
      if (Platform.OS == 'android') {
          BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
      }
    }

    handleBackPress = () => {
      this.fnMoveLogin();
      return true;
    }


    fnMoveToList() {
      Navigation.setRoot({
        root: {
          stack: {
            children: [
              {
                component: {
                  name: 'list',
                }
              }
            ]
          }
        }
      });
    }

    fnMoveLogin(){
        Navigation.setRoot({
            root: {
            stack: {
                children: [
                {
                component: {
                    name: 'map',
                    }
                }
                ]
            }
            }
        });
    }

    fnMoveToDetails(){
      Navigation.setRoot({
          root: {
            stack: {
              children: [
                {
                  component: {
                    name: 'details',
                    passProps: {
                      estado: this.state.estado,
                    }
                  }
                }
              ]
            }
          }
        });
    } 

    
    render() {
        return (
          <View style={{ flex: 1 }}>
          <Appbar.Header
                style={{backgroundColor: '#585858'}}>
                  <Appbar.Content
                    color='white'
                    title="Mapa"
                    subtitle="Mapa"
                  />
                  <Appbar.Action color='#58A085' icon="format-list-checkbox" onPress={this._handleList} />
          </Appbar.Header>
          <View style={styles.container}>
            <MapView
            onPress={() => { this.setState({ pinSelected: false})}}
              provider={PROVIDER_GOOGLE}
              style={styles.map}
              initialRegion={{
                latitude: LATITUDE,
                longitude: LONGITUDE,
                latitudeDelta: LATITUDE_DELTA,
                longitudeDelta: LONGITUDE_DELTA,
              }}
            >
            <MapView.Marker
              coordinate={{latitude: 21.8526073,
              longitude: -102.2332855}}
              title={"Aguascalientes"}
              description={"Aguascalientes"}
              onPress={() => { this.setState({ pinSelected: true, estado: 'Aguascalientes'})}}
            />
            <MapView.Marker
              coordinate={{latitude: 20.6737777,
              longitude: -103.4054532}}
              title={"Guadalajara"}
              description={"Jalisco"}
              onPress={() => { this.setState({ pinSelected: true, estado: 'Guadalajara'})}}
            />
            <MapView.Marker
              coordinate={{latitude: 21.1218994,
              longitude: -101.7360514}}
              title={"Leon"}
              description={"Guanajuato"}
              onPress={() => { this.setState({ pinSelected: true, estado: 'Leon'})}}
            />
            <MapView.Marker
              coordinate={{latitude: 20.6775741,
              longitude: -101.3768224}}
              title={"Irapuato"}
              description={"Guanajuato"}
              onPress={() => { this.setState({ pinSelected: true, estado: 'Irapuato'})}}
            />
            <MapView.Marker
              coordinate={{latitude: 19.7036519,
              longitude: -101.2411434}}
              title={"Morelia"}
              description={"Michoacan"}
              onPress={() => { this.setState({ pinSelected: true, estado: 'Morelia'})}}
            />
            </MapView>
            {this.state.pinSelected == true ?
              <TouchableOpacity style={styles.overlay} onPress={() => this.fnMoveToDetails()}>
                <Text style={styles.text}>Detalles de Clima</Text>
                <Text style={styles.subtitle}>({this.state.estado})</Text>
              </TouchableOpacity>
            : null}
          </View>
        </View>
        );
      }
        
}




const styles = StyleSheet.create({
    container: {
      ...StyleSheet.absoluteFillObject,
      top: 55,
      justifyContent: 'flex-end',
      alignItems: 'center'
    },
    map: {
      ...StyleSheet.absoluteFillObject,
    },
    overlay: {
      padding: 20,
      borderRadius: 10,
      borderWidth: 1,
      borderColor: 'gray',
      backgroundColor: '#22B8D6',
      position: 'absolute',
      bottom: 50,
      width: '90%',
      alignContent: 'center',
    },
    text: {
      color: 'white',
      alignContent: 'center',
      textAlign: 'center',
      fontWeight: 'bold',
      fontSize: 20,
    },
    subtitle: {
      color: 'white',
      alignContent: 'center',
      textAlign: 'center',
    }
  });

function mapDispatchToProps(dispatch) {
    return bindActionCreators(ActionCreators, dispatch);
}

export default connect(
    //mapStateToProps,
    mapDispatchToProps
)(Mapa);

