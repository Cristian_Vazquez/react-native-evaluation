import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Navigation } from 'react-native-navigation';
import {Appbar} from 'react-native-paper';
import { ActionCreators } from '../actions/index';
import { bindActionCreators } from 'redux';
import {
  StyleSheet,
  View,
  BackHandler,
  Dimensions,
} from 'react-native';

import { List } from 'react-native-paper';



class Lista extends Component {

    _handleMap = () => this.fnMoveToMap();

    static propTypes = {
        componentId: PropTypes.string
    };


    constructor(props) {
        super(props);
        
        Navigation.mergeOptions(this.props.componentId, {
            topBar: {
                visible: false,
                drawBehind: false
            }
        });
    }

    
    

    componentDidMount() {
      
    }

    componentWillUnmount() {
      if (Platform.OS == 'android') {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
      }
      if (this.navigationEventListener) {
          this.navigationEventListener.remove();
      }
    }

    componentDidAppear() {
      if (Platform.OS == 'android') {
          BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
      }
    }

    handleBackPress = () => {
      this.fnMoveToMap();
      return true;
    }


    fnMoveToMap() {
      Navigation.setRoot({
        root: {
          stack: {
            children: [
              {
                component: {
                  name: 'map',
                }
              }
            ]
          }
        }
      });
    }

    fnMoveToDetails(estado){
        Navigation.setRoot({
            root: {
              stack: {
                children: [
                  {
                    component: {
                      name: 'details',
                      passProps: {
                        estado: estado,
                      }
                    }
                  }
                ]
              }
            }
          });
    }


    
    render() {
        return (
          <View style={{ flex: 1 }}>
            <Appbar.Header
                    style={{backgroundColor: '#585858'}}>
                    <Appbar.Content
                        color='white'
                        title="Lista"
                        subtitle="Lista"
                    />
                    <Appbar.Action color='#58A085' icon="map" onPress={this._handleMap} />
            </Appbar.Header>
            <List.Item
                style={{borderWidth: 1, borderColor: 'gray'}}
                title="Aguascalientes"
                description="Aguascalientes"
                left={props => <List.Icon {...props} icon="map-marker" />}
                onPress={() => this.fnMoveToDetails('Aguascalientes')}
            />
            <List.Item
                style={{borderWidth: 1, borderColor: 'gray'}}
                title="Irapuato"
                description="Guanajuato"
                left={props => <List.Icon {...props} icon="map-marker" />}
                onPress={() => this.fnMoveToDetails('Irapuato')}
            />
            <List.Item
                style={{borderWidth: 1, borderColor: 'gray'}}
                title="Leon"
                description="Guanajuato"
                left={props => <List.Icon {...props} icon="map-marker" />}
                onPress={() => this.fnMoveToDetails('Leon')}
            />
            <List.Item
                style={{borderWidth: 1, borderColor: 'gray'}}
                title="Guadalajara"
                description="Jalisco"
                left={props => <List.Icon {...props} icon="map-marker" />}
                onPress={() => this.fnMoveToDetails('Guadalajara')}
            />
            <List.Item
                style={{borderWidth: 1, borderBottomWidth: 2, borderColor: 'gray'}}
                title="Morelia"
                description="Michoacan"
                left={props => <List.Icon {...props} icon="map-marker" />}
                onPress={() => this.fnMoveToDetails('Morelia')}
            />
          </View>
        );
      }
        
}




const styles = StyleSheet.create({
    
  });

function mapDispatchToProps(dispatch) {
    return bindActionCreators(ActionCreators, dispatch);
}

export default connect(
    //mapStateToProps,
    mapDispatchToProps
)(Lista);

