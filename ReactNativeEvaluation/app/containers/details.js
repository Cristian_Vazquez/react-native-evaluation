import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Navigation } from 'react-native-navigation';
import {Appbar, Avatar, Card, Title, Paragraph, DataTable } from 'react-native-paper';
import { ActionCreators } from '../actions/index';
import { bindActionCreators } from 'redux';
import {
  StyleSheet,
  View,
  BackHandler,
  Dimensions,
} from 'react-native';

import { List } from 'react-native-paper';



class Details extends Component {

    _handleMap = () => this.fnMoveToMap();

    static propTypes = {
        componentId: PropTypes.string
    };


    constructor(props) {
        super(props);

        this.state = {
            estado: '',
            objetoJsn: {
                'main': {
                    'temp_max': '-',
                    'temp': '-',
                    'temp_min': '-',
                    'humidity': '-',
                },
                'wind': {
                    'speed': '-',
                }
            },
        }
        
        this.state.estado = this.props.estado; 

        Navigation.mergeOptions(this.props.componentId, {
            topBar: {
                visible: false,
                drawBehind: false
            }
        });
    }

    
    

    componentDidMount() {
      this.props.dataWeather(this.props.estado);
    }

    componentWillUnmount() {
      if (Platform.OS == 'android') {
        BackHandler.removeEventListener('hardwareBackPress', this._handleMap);
      }
      if (this.navigationEventListener) {
          this.navigationEventListener.remove();
      }
    }

    componentWillReceiveProps(nextProps){
        if (nextProps.mapResponse != undefined) {
           this.state.objetoJsn.main.temp = nextProps.mapResponse.main.temp;
           this.state.objetoJsn.main.temp_min = nextProps.mapResponse.main.temp_min;
           this.state.objetoJsn.main.temp_max = nextProps.mapResponse.main.temp_max;
           this.state.objetoJsn.main.humidity = nextProps.mapResponse.main.humidity;
           this.state.objetoJsn.wind.speed = nextProps.mapResponse.wind.speed;
        }
    }

    componentDidAppear() {
      if (Platform.OS == 'android') {
          BackHandler.addEventListener('hardwareBackPress', this._handleMap);
      }
    }

    _handleMap = () => {
      this.fnMoveToMap();
      return true;
    }


    fnMoveToMap() {
      Navigation.setRoot({
        root: {
          stack: {
            children: [
              {
                component: {
                  name: 'map',
                }
              }
            ]
          }
        }
      });
    }


    
    render() {
        return (
          <View style={{ flex: 1 }}>
            <Appbar.Header
                    style={{backgroundColor: '#585858'}}>
                    <Appbar.BackAction
                        onPress={this._handleMap}
                        color='white'
                    />
                    <Appbar.Content
                        color='white'
                        title="Detalles"
                        subtitle="Detalles"
                    />
            </Appbar.Header>
            <View style={{flex: 1, alignSelf:'center'}}>
                <Avatar.Icon size={154} style={{backgroundColor: 'black', top: '20%'}} icon="city-variant-outline" />
            </View>
            <Card style={{bottom: '30%'}}>
                    <Card.Content >
                    <Title style={{alignContent: 'center', textAlign: 'center',}}>{this.state.estado}</Title>
                    <DataTable>
                        <DataTable.Row style={{borderBottomWidth: 0}}>
                            <DataTable.Cell style={{flex: 1}}><Avatar.Icon size={54} style={{backgroundColor: '#22B8D6'}} icon="weather-cloudy" /></DataTable.Cell>
                            <DataTable.Cell style={{flex: 3}}><Paragraph>Temperatura actual {this.state.objetoJsn.main.temp}°</Paragraph></DataTable.Cell>
                        </DataTable.Row>
                        <DataTable.Row style={{borderBottomWidth: 0}}>
                            <DataTable.Cell><Paragraph>Maxima {this.state.objetoJsn.main.temp_min}°</Paragraph></DataTable.Cell>
                            <DataTable.Cell><Paragraph>Minima {this.state.objetoJsn.main.temp_max}°</Paragraph></DataTable.Cell>
                            <DataTable.Cell><Paragraph>Humedad {this.state.objetoJsn.main.humidity}%</Paragraph></DataTable.Cell>
                        </DataTable.Row>
                        <DataTable.Row style={{borderBottomWidth: 0}}>
                            <DataTable.Cell style={{flex: 1}}><Avatar.Icon size={54} style={{backgroundColor: '#22B8D6'}} icon="weather-windy" /></DataTable.Cell>
                            <DataTable.Cell style={{flex: 3}}><Paragraph>Velicidad del viento {this.state.objetoJsn.wind.speed} km/h</Paragraph></DataTable.Cell>
                        </DataTable.Row>
                    </DataTable>
                    
                    </Card.Content>
            </Card>
          </View>
        );
      }
        
}




const styles = StyleSheet.create({
    
  });


  function mapStateToProps(state) {
    return {
      mapResponse: state.mapResponse,
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(ActionCreators, dispatch);
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Details);

