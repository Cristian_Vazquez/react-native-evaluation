import { combineReducers } from 'redux';
import * as mapReducer from './general';
export default combineReducers(Object.assign(
    mapReducer
));