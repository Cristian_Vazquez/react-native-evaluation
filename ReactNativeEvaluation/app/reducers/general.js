import createReducer from '../lib/createReducers'
import * as types from '../actions/types'



export const mapResponse = createReducer({}, {
  [types.SET_MAP_RESPONSE](state, action){
    let newState = {};
    newState = action.mapResponse;
    return newState;
  },
  [types.SET_CLEAN](state, action){
    let newState = {};
    newState = 0;
    return newState;
  }
});