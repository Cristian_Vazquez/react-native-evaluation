const Direcciones = {
    "General":"6001"
  };
  //const GlobalHost = "http://192.168.1.77"
  const GlobalHost = "http://ec2-18-221-10-183.us-east-2.compute.amazonaws.com"
  class Api {
      static headers() {
        return {
          'Content-Type': 'application/json',
          'strToken':''
        }
      }
      static get(direction,route,strToken) {
        return this.xhr(direction,route, null, 'GET',strToken);
      }
    
      static put(direction,route, params,strToken) {
        return this.xhr(direction,route, params, 'PUT',strToken)
      }
    
      static post(direction,route, params,strToken) {
        return this.xhr(direction,route, params, 'POST',strToken)
      }
    
      static delete(direction,route, params,strToken) {
        return this.xhr(direction,route, params, 'DELETE',strToken)
      }
    
      static xhr(direction,route, params, verb,strToken) {
        const host = GlobalHost;
        const url = `${route}`;
        let options = Object.assign({ method: verb }, params ? { body: JSON.stringify(params) } : null );
        options.headers = Api.headers()
        options.headers.strToken = strToken;
        return fetch(url, options).then( resp => {
          let json = resp.json();
          if (resp.ok) {
            return json
          }
          return json.then(err => {throw err;});
        }).then( json => json );
      }
    }
    export default Api